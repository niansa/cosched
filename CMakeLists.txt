cmake_minimum_required(VERSION 3.5)

project(cosched LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_library(cosched SHARED
    scheduler.cpp include/scheduler.hpp
    scheduled_thread.cpp include/scheduled_thread.hpp
    include/scheduler_mutex.hpp
    basic-coro/SingleEvent.cpp
)
target_include_directories(cosched PUBLIC include/ basic-coro/include/)
set_target_properties(cosched PROPERTIES POSITION_INDEPENDENT_CODE ON)

#add_executable(test test.cpp)
#target_link_libraries(test PRIVATE cosched)

install(TARGETS cosched
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
